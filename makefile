SRC = pyupdates
PYTHON = python3

.PHONY = clean-pyc clean-build

clean-pyc:
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '*.pyo' -exec rm --force {} +
	rm --force --recursive ${SRC}/__pycache__
	rm --force --recursive ${SRC}/repos/__pycache__
	@printf "\n"

clean-build:
	rm --force --recursive build/
	rm --force --recursive dist/
	rm --force --recursive *.egg-info
	@printf "\n"