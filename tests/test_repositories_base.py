from typing import Optional
from unittest import TestCase, mock

from pyupdates.repos.base import Base


class BaseImplementation(Base):
    def __init__(self, cmd: str, valid_commands: Optional[list[str]] = None) -> None:
        super().__init__(cmd, valid_commands)


class Test_Base(TestCase):
    data_1 = (
        "ImageMagick.x86_64                       1:6.9.12.70-1.fc36        updates"
        + "\nImageMagick-c++.x86_64                   1:6.9.12.70-1.fc36        updates"
    )

    @mock.patch.multiple(Base, __abstractmethods__=set())
    @mock.patch.object(Base, "__init__", return_value=None)
    def test_installedPacketManagers(self, mock_base_init):
        base = Base()  # type: ignore
        valid_commands = base._valid_commands = ["sh"]

        expected = ["sh"]

        self.assertEqual(valid_commands, expected)

    @mock.patch.multiple(Base, __abstractmethods__=set())
    @mock.patch.object(Base, "_run", return_value=data_1)
    @mock.patch.object(Base, "_isCmdInstalled", return_value=None)
    def test_separateRawData_1(self, mock_isCmdInstalled, mock_raw_output):
        base = Base("sh")  # type:ignore
        base._raw_output = "a\nb"

        ab = base._separateRawData(",")  # type:ignore

        expected = [["a"], ["b"]]

        self.assertEqual(ab, expected)
