from unittest import TestCase, mock

from pyupdates.repos.dnf import Dnf


class Test_Dnf(TestCase):
    data_1 = (
        "ImageMagick.x86_64                       1:6.9.12.70-1.fc36        updates"
        + "\nImageMagick-c++.x86_64                   1:6.9.12.70-1.fc36        updates"
    )

    data_2 = (
        "ImageMagick.x86_64                       1:6.9.12.70-1.fc36        updates"
        + "\nImageMagick-c++.x86_64                   1:6.9.12.70-1.fc36        updates"
        + "\nObsoleting Packages"
        + "\ngrub2-tools.x86_64                       1:2.06-59.fc36            updates"
        + "\ngrub2-tools.x86_64                   1:2.06-57.fc36            @updates"
        + "\ngrub2-tools-efi.x86_64                   1:2.06-59.fc36            updates"
        + "\ngrub2-tools.x86_64                   1:2.06-57.fc36            @updates"
    )

    data_3 = (
        "\nObsoleting Packages"
        + "\ngrub2-tools.x86_64                       1:2.06-59.fc36            updates"
        + "\ngrub2-tools.x86_64                   1:2.06-57.fc36            @updates"
        + "\ngrub2-tools-efi.x86_64                   1:2.06-59.fc36            updates"
        + "\ngrub2-tools.x86_64                   1:2.06-57.fc36            @updates"
    )

    @mock.patch.object(Dnf, "_run", return_value=data_1)
    @mock.patch.object(Dnf, "_isCmdInstalled", return_value=None)
    def test_separateRawData_1(self, mock_isCmdInstalled, mock_raw_output):
        dnf = Dnf()
        dnf.query()

        expected = [
            ["ImageMagick", "x86_64", "1:6.9.12.70-1", "fc36", "updates"],
            ["ImageMagick-c++", "x86_64", "1:6.9.12.70-1", "fc36", "updates"],
        ]

        self.assertEqual(dnf._update_list, expected)

    @mock.patch.object(Dnf, "_run", return_value=data_2)
    @mock.patch.object(Dnf, "_isCmdInstalled", return_value=None)
    def test_separateRawData_2(self, mock_isCmdInstalled, mock_raw_output):
        dnf = Dnf()
        dnf.query()

        expected = [
            [
                "grub2-tools",
                "x86_64",
                "1:2.06-59",
                "fc36",
                "updates",
                "grub2-tools",
                "x86_64",
                "1:2.06-57",
                "fc36",
                "@updates",
            ],
            [
                "grub2-tools-efi",
                "x86_64",
                "1:2.06-59",
                "fc36",
                "updates",
                "grub2-tools",
                "x86_64",
                "1:2.06-57",
                "fc36",
                "@updates",
            ],
        ]

        self.assertEqual(dnf._obsolete_list, expected)

    @mock.patch.object(Dnf, "_run", return_value=data_3)
    @mock.patch.object(Dnf, "_isCmdInstalled", return_value=None)
    def test_separateRawData_3(self, mock_isCmdInstalled, mock_raw_output):
        dnf = Dnf()
        dnf.query()

        expected = [
            [
                "grub2-tools",
                "x86_64",
                "1:2.06-59",
                "fc36",
                "updates",
                "grub2-tools",
                "x86_64",
                "1:2.06-57",
                "fc36",
                "@updates",
            ],
            [
                "grub2-tools-efi",
                "x86_64",
                "1:2.06-59",
                "fc36",
                "updates",
                "grub2-tools",
                "x86_64",
                "1:2.06-57",
                "fc36",
                "@updates",
            ],
        ]

        self.assertEqual(dnf._obsolete_list, expected)

    @mock.patch.object(Dnf, "_run", return_value=data_1)
    @mock.patch.object(Dnf, "_isCmdInstalled", return_value=None)
    def test_splitDataRow(self, mock_run, mock_isCmdInstalled):

        data = (
            "ImageMagick.x86_64                       1:6.9.12.70-1.fc36        updates"
        )

        dnf = Dnf()
        data_row = dnf._Dnf__splitDataRow(data)  # type: ignore

        expected = ["ImageMagick", "x86_64", "1:6.9.12.70-1", "fc36", "updates"]

        self.assertEqual(data_row, expected)
