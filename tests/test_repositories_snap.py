from unittest import TestCase, mock

from pyupdates.repos.snap import Snap


class Test_Snap(TestCase):
    data = (
        "Name    Version   Rev   Size  Publisher   Notes"
        + "\ncore           16-2.45.1+git2022.b6b3c25  9584  2679  canonical✓    core"
        + "\nget-iplayer    3.26                       250  2679   snapcrafters"
    )

    @mock.patch.object(Snap, "_run", return_value=data)
    @mock.patch.object(Snap, "_isCmdInstalled", return_value=None)
    def test_separateRawData(self, mock_isCmdInstalled, mock_raw_output):
        snap = Snap()
        snap.query()

        expected = [
            ["core", "16-2.45.1+git2022.b6b3c25", "9584", "2679", "canonical✓", "core"],
            ["get-iplayer", "3.26", "250", "2679", "snapcrafters"],
        ]

        self.assertEqual(snap._update_list, expected)
