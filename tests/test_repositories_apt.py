from unittest import TestCase, mock

from pyupdates.repos.apt import Apt


class Test_Apt(TestCase):
    data = (
        "base-files/stable 11.1+deb11u6 amd64 [upgradable from: 11.1+deb11u5]\n"
        + "distro-info-data/stable 0.51+deb11u3 all [upgradable from: 0.51+deb11u2]"
    )

    @mock.patch.object(Apt, "_run", return_value=data)
    @mock.patch.object(Apt, "_isCmdInstalled", return_value=None)
    def test_separateRawData(self, mock_isCmdInstalled, mock_raw_output):
        apt = Apt()
        apt.query()

        expected = [
            ["base-files", "stable", "11.1", "amd64", "11.1", "deb11u5"],
            ["distro-info-data", "stable", "0.51", "all", "0.51", "deb11u2"],
        ]

        self.assertEqual(apt._update_list, expected)
