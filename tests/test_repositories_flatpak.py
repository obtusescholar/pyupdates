from unittest import TestCase, mock

from pyupdates.repos.flatpak import Flatpak


class Test_Flatpak(TestCase):
    data = (
        "Bitwarden	com.bitwarden.desktop	2022.11.0	stable	x86_64"
        + "\nBrave Browser	com.brave.Browser	1.46.140	stable	x86_64"
    )

    @mock.patch.object(Flatpak, "_run", return_value=data)
    @mock.patch.object(Flatpak, "_isCmdInstalled", return_value=None)
    def test_separateRawData(self, mock_isCmdInstalled, mock_raw_output):
        flatpak = Flatpak()
        flatpak.query()

        expected = [
            ["Bitwarden", "com.bitwarden.desktop", "2022.11.0", "stable", "x86_64"],
            ["Brave Browser", "com.brave.Browser", "1.46.140", "stable", "x86_64"],
        ]

        self.assertEqual(flatpak._update_list, expected)
