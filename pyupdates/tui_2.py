#! /usr/bin/env python3


import curses
from dataclasses import dataclass
from enum import Enum, EnumMeta
from time import sleep
from typing import Optional, Tuple, Union

from pytools.logtools import getLogger

from pyupdates import definitions
from pyupdates.repos.apt import Apt
from pyupdates.repos.base import Base
from pyupdates.repos.dnf import Dnf
from pyupdates.repos.flatpak import Flatpak
from pyupdates.repos.snap import Snap

logger = getLogger()


def init():
    curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_YELLOW)
    curses.init_pair(5, curses.COLOR_BLACK, curses.COLOR_WHITE)

    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLACK)

    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_WHITE, curses.COLOR_YELLOW)
    curses.init_pair(7, curses.COLOR_WHITE, curses.COLOR_BLUE)

    class CursesColors(EnumMeta):
        WHITE_BLACK = curses.color_pair(1)
        WHITE_YELLOW = curses.color_pair(4)
        WHITE_BLUE = curses.color_pair(5)

        BLACK_WHITE = curses.color_pair(7)
        BLACK_YELLOW = curses.color_pair(6)

        YELLOW_BLACK = curses.color_pair(2)
        BLUE_BLACK = curses.color_pair(3)

    return CursesColors


class Keys(EnumMeta):
    LEFT = (260, 546, "h", "H")
    DOWN = (258, 526, "j", "J")
    UP = (259, 567, "k", "K")
    RIGHT = (261, 561, "l", "L")
    ENTER = ("\n", "\r\n")
    TAB = ("\t",)
    QUIT = ("q", "Q")


class Curses:
    def __init__(self) -> None:
        self.stdscr = None

    def __enter__(self):
        try:
            self.open()
            return self.stdscr
        except:
            self.close()
            raise

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close()

    def open(self):
        self.stdscr = curses.initscr()

        curses.curs_set(0)

        curses.noecho()
        curses.cbreak()

        self.stdscr.keypad(True)

        try:
            curses.start_color()
        except:
            pass

    def close(self):
        if self.stdscr is not None:
            curses.curs_set(1)
            self.stdscr.keypad(False)
            curses.echo()
            curses.nocbreak()
            curses.endwin()


class RepoEnum(Enum):
    DNF = Dnf
    FLATPAK = Flatpak
    SNAP = Snap
    APT = Apt

    def isValid(self):
        return True if self.value.installedPacketManagers() else False


class Strings(EnumMeta):
    TITLE_ALL = "All"
    TITLE_REPOS = "Repositories"
    TITLE_UPDATES = "Updates"
    REPO_NAMES = [repo_enum.value.name for repo_enum in RepoEnum if repo_enum.isValid()]


class Point:
    def __init__(self, y: int, x: int) -> None:
        self.__y = y
        self.__x = x

    @property
    def y(self) -> int:
        return self.__y

    @property
    def x(self) -> int:
        return self.__x

    def __eq__(self, other) -> bool:
        if isinstance(other, Point):
            return self.y == other.y and self.x == other.x
        elif isinstance(other, tuple):
            return self.y == other[0] and self.x == other[1]
        else:
            raise NotImplementedError(
                f"Point __eq__ not implemented for '{type(other)}'."
            )

    def __repr__(self) -> str:
        return f"Point(y={self.y}, x={self.x})"


class Rectangle:
    def __init__(self, origo: Point, corner: Point) -> None:
        self.__origo = origo
        self.__corner = corner

    @property
    def origo(self) -> Point:
        return self.__origo

    @property
    def corner(self) -> Point:
        return self.__corner

    @property
    def size(self) -> Point:
        y = self.corner.y - self.origo.y
        x = self.corner.x - self.origo.x
        return Point(y, x)

    def __repr__(self):
        return f"Rectangle(origo={self.origo}, corner={self.corner})"


class TabMeasurements:
    def __init__(self, stdscr) -> None:
        self.__origo_yx = Point(0, 0)
        self.__repo_range_x = (20, 50)
        self.__prepend_yx = Point(2, 3)
        self.resize(stdscr)

    def hasResized(self, stdscr) -> bool:
        if self.__corner_yx == stdscr.getmaxyx():
            return False
        else:
            return True

    def resize(self, stdscr) -> None:
        self.__corner_yx = Point(*stdscr.getmaxyx())

    @property
    def size(self) -> Point:
        return self.__corner_yx

    @property
    def prepend(self) -> Point:
        return self.__prepend_yx

    @property
    def repo(self) -> Rectangle:
        repo_min, repo_max = self.__repo_range_x
        corner = Point(
            self.__corner_yx.y,
            min(max(self.__corner_yx.x // 4, repo_min), repo_max),
        )
        return Rectangle(self.__origo_yx, corner)

    @property
    def updates(self) -> Rectangle:
        repo_corner = self.repo.corner
        updates_origo = Point(self.__origo_yx.y, repo_corner.x)
        return Rectangle(updates_origo, self.__corner_yx)


class RepoTab:
    def __init__(self, pad, stdscr) -> None:
        self.__repos = self.__getValidRepos()
        self.__pad = pad
        self.__meas = TabMeasurements(stdscr)

        self.initialize()
        self.refresh()

    def __getValidRepos(self):
        repos = []
        for repo in RepoEnum:
            if repo.value.installedPacketManagers():
                repos.append(repo)
        return repos

    def initialize(self):
        self.__pad.erase()
        for i, repo in enumerate(RepoEnum):
            if repo.isValid():
                self.__pad.addstr(
                    self.__meas.prepend.y + i,
                    self.__meas.prepend.x,
                    f"{repo.value.name}",
                )

    def refresh(self):
        self.__pad.refresh(
            0,
            0,
            self.__meas.repo.origo.y,
            self.__meas.repo.origo.x,
            self.__meas.repo.corner.y,
            self.__meas.repo.corner.x,
        )


def initRepoData(meas, repo_pad):
    for i, repo in enumerate(RepoEnum):
        if repo.isValid():
            repo_pad.addstr(meas.prepend.y + i, meas.prepend.x, f"{repo.value.name}")


def getTabs(
    stdscr,
    meas,
    repo_pad=None,
    updates_pad=None,
) -> tuple:
    def refreshTabs(meas, repo_pad, updates_pad):
        repo_pad.refresh(
            0,
            0,
            meas.repo.origo.y,
            meas.repo.origo.x,
            meas.repo.corner.y,
            meas.repo.corner.x,
        )
        updates_pad.refresh(
            0,
            0,
            meas.updates.origo.y,
            meas.updates.origo.x,
            meas.updates.corner.y,
            meas.updates.corner.x,
        )

    if repo_pad is None:
        repo_pad = curses.newpad(meas.repo.size.y, meas.repo.size.x)
        initRepoData(meas, repo_pad)
    if updates_pad is None:
        updates_pad = curses.newpad(meas.updates.size.y, meas.updates.size.x)

    if meas.hasResized(stdscr):
        meas.resize(stdscr)
        repo_pad.resize(meas.repo.size.y, meas.repo.size.x)
        updates_pad.resize(meas.updates.size.y, meas.updates.size.x)

        repo_pad.erase()
        updates_pad.erase()

        initRepoData(meas, repo_pad)

    stdscr.refresh()

    repo_pad.border()
    updates_pad.border()

    refreshTabs(meas, repo_pad, updates_pad)

    return repo_pad, updates_pad


def main(stdscr) -> None:
    repo_pad = None
    updates_pad = None
    meas = TabMeasurements(stdscr)
    stdscr.erase()

    while True:
        repo_pad, updates_pad = getTabs(stdscr, meas, repo_pad, updates_pad)

        key = stdscr.getch()

        if key in Keys.QUIT or chr(key) in Keys.QUIT:
            return

        sleep(0.025)


if __name__ == "__main__":
    with Curses() as stdscr:
        if stdscr is not None:
            main(stdscr)
