#! /usr/bin/env python3


import curses
from dataclasses import dataclass
from enum import Enum, EnumMeta
from pathlib import Path
from time import sleep
from typing import Optional, Union

from pytools.logtools import getLogger

from pyupdates import definitions
from pyupdates.tui import tabs
from pyupdates.tui.curse import Stdscr
from pyupdates.tui.keys_enum import Keys
from pyupdates.tui.tabs import repos_tab

logger = getLogger()


def main(stdscr):
    stdscr.erase()
    key = -1

    repos_pad = curses.newpad(1, 1)
    updates_pad = curses.newpad(1, 1)
    stdscr.refresh()

    repos_tab = tabs.ReposTab(stdscr, repos_pad)
    updates_tab = tabs.UpdatesTab(stdscr, updates_pad)

    repos_tab.initContent()
    updates_tab.initContent()

    while True:
        if key == curses.KEY_RESIZE:
            stdscr.refresh()
            repos_tab.resize(stdscr)
            updates_tab.resize(stdscr)

            repos_tab.refresh()
            updates_tab.refresh()

        key = stdscr.getch()

        if key in Keys.QUIT:
            return
        elif key == curses.KEY_RESIZE:
            continue
        elif key != -1:
            repos_pad.addstr(1, 5, f"key: {key}")
            repos_tab.refresh()

        sleep(0.20)


if __name__ == "__main__":
    with Stdscr() as stdscr:
        main(stdscr)
