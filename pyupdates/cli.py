#! /usr/bin/env python3


import argparse
from typing import Optional

from pytools.logtools import getLogger
from pytools.string_format import StringFormat

from pyupdates import definitions
from pyupdates.repos.apt import Apt
from pyupdates.repos.dnf import Dnf
from pyupdates.repos.flatpak import Flatpak
from pyupdates.repos.snap import Snap

logger = getLogger()


def _getArgs(arguments: Optional[list[str]] = None) -> argparse.Namespace:
    """Create argparser arguments

    Returns:
        argparse.Namespace: argparser arguments
    """

    valid_repos = _getValidRepos()

    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        prog="pyupdates",
        description=f"Get update info from Linux repositories. You have required packets to use: {', '.join(valid_repos)}.",
    )

    parser.add_argument("-V", "--version", action="store_true", help="project version")
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="be more verbose with update titles",
    )
    parser.add_argument("-d", "--dnf", action="store_true", help="dnf packet manager")
    parser.add_argument(
        "-f", "--flatpak", action="store_true", help="flatpak packet manager"
    )
    parser.add_argument("-s", "--snap", action="store_true", help="snap packet manager")
    parser.add_argument("-a", "--apt", action="store_true", help="apt packet manager")
    parser.add_argument(
        "-p", "--packet", action="store_true", help="outputs packet names only"
    )
    parser.add_argument(
        "-q",
        "--quantity",
        action="store_true",
        help="how many updates the output contains",
    )
    parser.add_argument(
        "-j", "--json", action="store_true", help="outputs data in json format"
    )
    parser.add_argument(
        "--list-packet-managers",
        action="store_true",
        help="list packet managers installed in system",
    )
    parser.add_argument(
        "--dnf-updates", action="store_true", help="output dnf updates only"
    )
    parser.add_argument(
        "--dnf-updates-packet",
        action="store_true",
        help="output dnf update package names only",
    )
    parser.add_argument(
        "--dnf-obsoletes",
        action="store_true",
        help="output dnf obsoleting packages only",
    )
    parser.add_argument(
        "--dnf-obsoletes-packet",
        action="store_true",
        help="output dnf obsoleting package names only",
    )
    parser.add_argument(
        "--dnf-json-updates",
        action="store_true",
        help="output dnf updates only as json",
    )
    parser.add_argument(
        "--dnf-json-obsoletes",
        action="store_true",
        help="output dnf obsoleting packages as json",
    )

    return parser.parse_args(arguments)


def _headerFormatting(header: definitions.ColorRepoEnum) -> str:
    return f"{StringFormat.BOLD}{header.value}{header.name}{StringFormat.END}"


def _dnf(args: argparse.Namespace) -> str:
    """DNF update argparse actions

    Args:
        args (argparse.Namespace): argparser arguments

    Returns:
        str: chosen update information
    """

    if args.verbose:
        print(_headerFormatting(definitions.ColorRepoEnum.DNF))

    dnf = Dnf()
    dnf.query()

    if args.packet:
        return str(dnf.updates.to_string()) + str(dnf.obsoletes.to_string())
    elif args.quantity:
        return str(len(dnf.updates) + len(dnf.obsoletes))
    elif args.json:
        return str(dnf.updates.to_json(orient="records")) + str(
            dnf.obsoletes.to_json(orient="records")
        )
    elif args.dnf_updates:
        return str(dnf.updates.to_string())
    elif args.dnf_updates_packet:
        return str(dnf.updates["Packet"].to_string())
    elif args.dnf_obsoletes:
        return str(dnf.obsoletes.to_string())
    elif args.dnf_obsoletes_packet:
        return str(
            dnf.obsoletes[
                ["Packet", "Version", "Old Packet", "Old Version"]
            ].to_string()
        )
    elif args.dnf_json_updates:
        return str(dnf.updates.to_json(orient="records"))
    elif args.dnf_json_obsoletes:
        return str(dnf.obsoletes.to_json(orient="records"))
    else:
        string = ""
        if args.verbose:
            string += f"{StringFormat.BOLD}Updates{StringFormat.END}\n"
        string += str(dnf.updates.to_string())
        if args.verbose:
            string += f"\n{StringFormat.BOLD}Obsoletes{StringFormat.END}"
        string += "\n" + str(dnf.obsoletes.to_string())
        return string


def _flatpak(args: argparse.Namespace) -> str:
    """Flatpak update argparse actions

    Args:
        args (argparse.Namespace): argparser arguments

    Returns:
        str: chosen update information
    """

    if args.verbose:
        print(_headerFormatting(definitions.ColorRepoEnum.FLATPAK))

    flatpak = Flatpak()
    flatpak.query()

    if args.packet:
        return str(flatpak.updates["Packet"].to_string())
    elif args.quantity:
        return str(len(flatpak.updates))
    elif args.json:
        return str(flatpak.updates.to_json(orient="records"))
    else:
        return str(flatpak.updates.to_string())


def _snap(args: argparse.Namespace) -> str:
    """snap update argparse actions

    Args:
        args (argparse.Namespace): argparser arguments

    Returns:
        str: chosen update information
    """

    if args.verbose:
        print(_headerFormatting(definitions.ColorRepoEnum.SNAP))

    snap = Snap()
    snap.query()

    if args.packet:
        return str(snap.updates["Packet"].to_string())
    elif args.quantity:
        return str(len(snap.updates))
    elif args.json:
        return str(snap.updates.to_json(orient="records"))
    else:
        return str(snap.updates.to_string())


def _apt(args: argparse.Namespace) -> str:
    """Apt update argparse actions

    Args:
        args (argparse.Namespace): argparser arguments

    Returns:
        str: chosen update information
    """

    if args.verbose:
        print(_headerFormatting(definitions.ColorRepoEnum.APT))

    apt = Apt()
    apt.query()

    if args.packet:
        return str(apt.updates["Packet"].to_string())
    elif args.quantity:
        return str(len(apt.updates))
    elif args.json:
        return str(apt.updates.to_json(orient="records"))
    else:
        return str(apt.updates.to_string())


def _getValidCommands() -> list[str]:
    valid_commands: list[str] = []
    valid_commands.extend(Dnf.installedPacketManagers())
    valid_commands.extend(Flatpak.installedPacketManagers())
    valid_commands.extend(Snap.installedPacketManagers())
    valid_commands.extend(Apt.installedPacketManagers())
    return valid_commands


def _getValidRepos() -> list[str]:
    valid_repos = []
    repos = [Dnf, Flatpak, Snap, Apt]

    for repo in repos:
        if repo.installedPacketManagers():
            valid_repos.append(repo.name)

    return valid_repos


def main(arguments: Optional[list[str]] = None) -> None:
    """Divide argparse actions to different packet manager sections"""

    args: argparse.Namespace = _getArgs(arguments)

    logger.debug(args)

    if args.version:
        print(definitions.__version__)

    if args.list_packet_managers:
        valid_commands = _getValidCommands()

        if args.verbose:
            print(f"{StringFormat.BOLD}INSTALLED PACKET MANAGERS{StringFormat.END}")
        for command in valid_commands:
            print(command)

    if args.dnf:
        print(_dnf(args))
    if args.flatpak:
        print(_flatpak(args))
    if args.snap:
        print(_snap(args))
    if args.apt:
        print(_apt(args))

    # if not any(args._get_args()):
    #    # print(args)
    #    # pass
    #    print(definitions.___version__)


if __name__ == "__main__":
    if True:
        main(["-h"])
