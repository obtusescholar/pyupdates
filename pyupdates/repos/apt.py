#! /usr/bin/env python3


from itertools import chain
from typing import Optional

from pytools.logtools import getLogger

from pyupdates.repos.base import Base


class Apt(Base):
    _update_columns: list[str] = [
        "Packet",
        "Revision",
        "Version",
        "Arch",
        "Old Version",
        "Old Distro",
    ]
    _valid_commands = ["apt", "apt-get"]
    _logger = getLogger()
    _name = "Apt"

    def __init__(
        self,
        cmd: str = "apt list --upgradeable -qq",
        valid_commands: Optional[list[str]] = None,
    ):
        # self._valid_commands = ["apt", "apt-get"]
        super().__init__(cmd, valid_commands)
        # self.query()

    def _separateRawData(self, delimeter: Optional[str] = None) -> list[list[str]]:
        update_lines: list[list[str]] = []
        if self._raw_output:
            for line in self._raw_output.strip().split("\n"):
                new, old, *_ = chain(line.split(" [upgradable from: "), ["" * 2])

                packetFull, versionFull, arch, *_ = chain(
                    new.split(delimeter), ["" * 3]
                )
                packet, rev, *_ = chain(packetFull.split("/"), ["" * 2])
                version, distro, *_ = chain(versionFull.split("+"), ["" * 2])

                if "+" in old:
                    oldVersion, oldDistro, *_ = chain(
                        old.replace("]", "").split("+"), ["" * 2]
                    )
                else:
                    oldVersion, oldDistro, *_ = chain(
                        old.replace("]", "").split("~"), ["" * 2]
                    )

                update_lines.append([packet, rev, version, arch, oldVersion, oldDistro])

        return update_lines

    def query(self) -> None:
        super().query()
        self._update_list = self._separateRawData()
        self._updates = self._makeDataFrame(self._update_list, self._update_columns)


if __name__ == "__main__":
    if True:
        apt: Apt = Apt("cat data/apt.txt", ["cat"])
        print(apt.updates)

        apt_empty: Apt = Apt("cat data/empty.txt", ["cat"])
        print(apt_empty.updates)
