#! /usr/bin/env python3


import subprocess
from typing import Optional

import pandas as pd
from pytools.logtools import getLogger

from pyupdates.repos.base import Base


class Dnf(Base):
    _update_columns = [
        "Packet",
        "Arch",
        "Version",
        "Distro",
        "Repo",
    ]
    _obsolete_columns = _update_columns + [
        "Old Packet",
        "Old Arch",
        "Old Version",
        "Old Distro",
        "Old Repo",
    ]
    _valid_commands = ["dnf", "yum"]
    _logger = getLogger()
    _name = "Dnf"

    def __init__(
        self,
        cmd: str = "dnf check-upgrade -q",
        valid_commands: Optional[list[str]] = None,
    ) -> None:
        # self._valid_commands = ["dnf", "yum"]
        super().__init__(cmd, valid_commands)
        # self.query()

    def _errorcheck(self, process: subprocess.CompletedProcess[bytes]) -> str:
        if process.returncode == 0 or process.returncode == 100:
            return str(process.stdout.decode())
        else:
            raise subprocess.CalledProcessError(
                returncode=process.returncode,
                cmd=process.args,
                stderr=process.stderr,
            )

    def query(self) -> None:
        super().query()
        self._update_list, self._obsolete_list = self.__rawToUpdatesObsoletes()
        self._updates = self._makeDataFrame(self._update_list, self._update_columns)
        self._obsoletes = self._makeDataFrame(
            self._obsolete_list, self._obsolete_columns
        )

    @staticmethod
    def __splitDataRow(data_str: str) -> list[str]:
        packetFull, versionFull, repo = data_str.split()
        packet, arch, *_ = *packetFull.rsplit(".", 1), "" * 2
        version, distro, *_ = *versionFull.rsplit(".", 1), "" * 2
        return [packet, arch, version, distro, repo]

    def __rawToUpdatesObsoletes(self) -> tuple[list[list[str]], list[list[str]]]:
        delimiter = "Obsoleting Packages"
        update_lines: list[list[str]] = []
        obsolete_lines: list[list[str]] = []

        if delimiter in self._raw_output:
            separated = self._raw_output.split(delimiter)

            for line in separated[0].strip().split("\n"):
                if line:
                    update_lines.append(self.__splitDataRow(line))

            for i, line in enumerate(separated[1].strip().split("\n")):
                if i % 2 == 0:
                    obsolete_lines.append(self.__splitDataRow(line))
                else:
                    obsolete_lines[-1].extend(self.__splitDataRow(line))
        elif self._raw_output:
            update_lines = [
                self.__splitDataRow(line)
                for line in self._raw_output.strip().split("\n")
            ]

        return (update_lines, obsolete_lines)

    @property
    def obsoletes(self) -> pd.DataFrame:
        """Obsoleting packets

        Returns:
            pd.DataFrame: pandas table of obsoleting packets
        """

        return self._obsoletes


if __name__ == "__main__":
    if True:
        commands = ["cat"]

        dnf: Dnf = Dnf("cat data/dnf_2.txt", valid_commands=["cat"])
        print(dnf.updates)
        print(dnf.obsoletes)

        dnf_empty: Dnf = Dnf("cat data/empty.txt", valid_commands=["cat"])
        print(dnf_empty.updates)
        print(dnf_empty.obsoletes)
