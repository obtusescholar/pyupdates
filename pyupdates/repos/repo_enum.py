#! /usr/bin/env python3


from enum import Enum

from pyupdates import repos


class RepoEnum(Enum):
    DNF = repos.Dnf
    FLATPAK = repos.Flatpak
    SNAP = repos.Snap
    APT = repos.Apt

    @classmethod
    def getValid(cls):
        return (repo for repo in RepoEnum if repo.isValid())

    def isValid(self):
        return True if self.value.installedPacketManagers() else False
