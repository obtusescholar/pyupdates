#! /usr/bin/env python3


import subprocess
from abc import ABC, abstractmethod
from typing import Any, Optional

import pandas as pd
from pytools.logtools import getLogger

from pyupdates.definitions import logtoolsSettings

logtoolsSettings()


class Base(ABC):
    """Base class for packet manager classes

    Args:
        ABC (ABC): Abstract Base Class

    Raises:
        NotImplementedError: given command is not supported
        subprocess.CalledProcessError: given command is not installed
    """

    _valid_commands: list[str] = []
    _update_columns: list[str] = []
    _logger = getLogger()
    _name: str

    @abstractmethod
    def __init__(self, cmd: str, valid_commands: Optional[list[str]] = None) -> None:
        if valid_commands is not None:
            self._valid_commands.extend(valid_commands)

        self._update_list: list[list[str]]
        self._updates: pd.DataFrame

        self._cmd = cmd
        self._binary: str = self._cmd.split()[0]
        self._isCmdInstalled()

    @classmethod
    def installedPacketManagers(cls) -> list[str]:
        installed = []

        for is_valid_command in cls._valid_commands:
            cmd_list = ["bash", "-c", f"command -v {is_valid_command}; exit_code=$?; exit $exit_code"]
            process: subprocess.CompletedProcess[bytes] = subprocess.run(
                cmd_list, capture_output=True
            )

            if process.returncode == 0:
                installed.append(is_valid_command)

        return installed

    def _run(self, cmd: str) -> str:
        self._logger.debug(f"_run() cmd:'{cmd}'")

        if self._binary in self._valid_commands:
            process: subprocess.CompletedProcess[bytes] = subprocess.run(
                cmd.split(), capture_output=True
            )
            return self._errorcheck(process)

        self._logger.critical("command:'{cmd}' is not valid")

        raise NotImplementedError(
            f"Command was:'{cmd}', valid commands are:'{self._valid_commands}'"
        )

    def query(self) -> None:
        """Retrieve update information"""

        self._raw_output: str = self._run(self._cmd) or ""
        self._logger.debug(f"query() raw_output:'{self._raw_output}'")
        # self._update_list: list[list[str]]
        # self._updates: pd.DataFrame

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(cmd='{self._cmd}', columns='self.__columns')"

    def _isCmdInstalled(self) -> None:
        cmd_list = ["bash", "-c", f"command -v {self._binary}; exit_code=$?; exit $exit_code"]
        _: subprocess.CompletedProcess[bytes] = subprocess.run(
            cmd_list, check=True, capture_output=True
        )

    def _errorcheck(self, process: subprocess.CompletedProcess[bytes]) -> str:
        self._logger.debug(f"_errorcheck process:'{process}'")

        if process.returncode == 0:
            return process.stdout.decode()
        else:
            self._logger.warning("_errorcheck returncode was not 0")

            raise subprocess.CalledProcessError(
                returncode=process.returncode,
                cmd=process.args,
                stderr=process.stderr,
            )

    def _separateRawData(self, delimeter: Optional[str] = None) -> list[list[str]]:
        update_lines: list[list[str]] = []

        if self._raw_output:
            for line in self._raw_output.strip().split("\n"):
                update_lines.append(line.split(delimeter))

        self._logger.debug(
            f"_separateRawData delimiter:'{delimeter}', _raw_output:'{self._raw_output}'"
        )

        return update_lines

    @staticmethod
    def _makeDataFrame(data_list: list[Any], columns: list[str]) -> pd.DataFrame:
        return pd.DataFrame(data=data_list, columns=columns)

    @property
    def raw_output(self) -> str:
        """Raw data from packet manager

        Returns:
            str: unmodified output string
        """

        return self._raw_output

    @property
    def updates(self) -> pd.DataFrame:
        """Update packets

        Returns:
            pd.DataFrame: pandas table of update packets
        """

        return self._updates

    @classmethod
    @property
    def name(cls) -> str:
        return cls._name


if __name__ == "__main__":
    pass
