#! /usr/bin/env python3


import subprocess
from typing import Optional

from pytools.logtools import getLogger

from pyupdates.repos.base import Base


class Flatpak(Base):
    _update_columns = ["Packet", "Source", "Version", "Channel", "Arch"]
    _valid_commands = ["flatpak"]
    _logger = getLogger()
    _name = "Flatpak"

    def __init__(
        self,
        cmd: str = "flatpak remote-ls --updates",
        valid_commands: Optional[list[str]] = None,
    ):
        # self._valid_commands = ["flatpak"]
        super().__init__(cmd, valid_commands)
        # self.query()

    def query(self) -> None:
        super().query()
        self._update_list = self._separateRawData(delimeter="\t")
        self._updates = self._makeDataFrame(self._update_list, self._update_columns)

    # def _errorcheck(self, process: subprocess.CompletedProcess[bytes]) -> str:
    #    if process.returncode == 0 or process.returncode == 1:
    #        return process.stdout.decode()
    #    else:
    #        raise subprocess.CalledProcessError(
    #            returncode=process.returncode,
    #            cmd=process.args,
    #            stderr=process.stderr,
    #        )


if __name__ == "__main__":
    if True:
        commands = ["cat"]

        flatpak: Flatpak = Flatpak("cat data/flatpak.txt", ["cat"])
        print(flatpak.updates)

        flatpak_empty: Flatpak = Flatpak("cat data/empty.txt", ["cat"])
        print(flatpak_empty.updates)
