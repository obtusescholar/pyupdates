#! /usr/bin/env python3


from typing import Optional

from pytools.logtools import getLogger

from pyupdates.repos.base import Base


class Snap(Base):
    _update_columns = ["Packet", "Version", "Rev", "Size", "Publisher", "Notes"]
    _valid_commands = ["snap"]
    _logger = getLogger()
    _name = "Snap"

    def __init__(
        self,
        cmd: str = "snap refresh --list",
        valid_commands: Optional[list[str]] = None,
    ):
        # self._valid_commands = ["snap"]
        super().__init__(cmd, valid_commands)
        # self.query()

    def query(self) -> None:
        super().query()
        self._update_list = self._separateRawData()[1:]
        self._updates = self._makeDataFrame(self._update_list, self._update_columns)


if __name__ == "__main__":
    if True:
        commands = ["cat"]

        snap: Snap = Snap("cat data/snap.txt", ["cat"])
        print(snap.updates)

        snap_empty: Snap = Snap("cat data/empty.txt", ["cat"])
        print(snap_empty.updates)
