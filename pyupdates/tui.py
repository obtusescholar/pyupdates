#! /usr/bin/env python3


import curses
from enum import EnumMeta
from time import sleep

from pytools.logtools import getLogger

from pyupdates import definitions
from pyupdates.repos.apt import Apt
from pyupdates.repos.dnf import Dnf
from pyupdates.repos.flatpak import Flatpak
from pyupdates.repos.snap import Snap

logger = getLogger()


class Curse:
    def __init__(self, fn) -> None:
        self.__fn = fn

    def __enter__(self):
        curses.wrapper(self.__fn)  # type: ignore

    def __exit__(self, exc_type, exc_value, exc_traceback):
        curses.curs_set(1)


class Strings(EnumMeta):
    ALL = "All"
    REPOSITORIES = "Repositories"
    UPDATES = "Updates"


def _getValidRepos():
    valid_repos = []
    repos = [Dnf, Flatpak, Snap, Apt]

    for repo in repos:
        if repo.installedPacketManagers():
            valid_repos.append(repo)

    return valid_repos


def tabs(stdscr):
    repo_tab_min = 20
    repo_tab_max = 50

    PREPEND = 3
    REPOS_TAB_WIDTH = min(max(stdscr.getmaxyx()[1] // 4, repo_tab_min), repo_tab_max)
    UPDATES_TAB_WIDTH = stdscr.getmaxyx()[1] - REPOS_TAB_WIDTH

    return (PREPEND, REPOS_TAB_WIDTH, UPDATES_TAB_WIDTH)


def colors():
    # Black
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_BLACK, curses.COLOR_YELLOW)
    curses.init_pair(7, curses.COLOR_BLACK, curses.COLOR_WHITE)

    # White
    curses.init_pair(4, curses.COLOR_WHITE, curses.COLOR_YELLOW)
    curses.init_pair(5, curses.COLOR_WHITE, curses.COLOR_BLUE)

    class Colors(EnumMeta):
        # Black
        WHITE_BLACK = curses.color_pair(1)
        YELLOW_BLACK = curses.color_pair(2)
        BLUE_BLACK = curses.color_pair(3)
        BLACK_YELLOW = curses.color_pair(6)
        BLACK_WHITE = curses.color_pair(7)

        # White
        WHITE_YELLOW = curses.color_pair(4)
        WHITE_BLUE = curses.color_pair(5)

    return Colors


def updatesTab(stdscr):
    Colors = colors()
    PREPEND, REPOS_TAB_WIDTH, UPDATES_TAB_WIDTH = tabs(stdscr)

    tab = stdscr.subwin(0, 0, 0, REPOS_TAB_WIDTH)
    tab.border()

    pad = curses.newpad(1, 1)

    tab.addstr(0, PREPEND, Strings.UPDATES, Colors.YELLOW_BLACK | curses.A_BOLD)
    tab.refresh()

    return tab, pad


def reposTab(stdscr):
    Colors = colors()
    PREPEND, REPOS_TAB_WIDTH, UPDATES_TAB_WIDTH = tabs(stdscr)

    tab = stdscr.subwin(0, REPOS_TAB_WIDTH, 0, 0)
    tab.erase()
    tab.border()

    pad = curses.newpad(1, 1)

    tab.addstr(0, PREPEND, Strings.REPOSITORIES, Colors.YELLOW_BLACK | curses.A_BOLD)
    for i, repo in enumerate(_getValidRepos()):
        tab.addstr(2 + i, PREPEND, repo.name)

    return tab, pad


def _selectTab(stdscr, tab_dict_list, selected_tab):
    Colors = colors()
    PREPEND, _, _ = tabs(stdscr)

    for i, tab_dict in enumerate(tab_dict_list):
        if i == selected_tab:
            tab_dict["tab"].chgat(
                0,
                PREPEND,
                len(tab_dict["label"]),
                Colors.WHITE_YELLOW | curses.A_BOLD,
            )
        else:
            tab_dict["tab"].chgat(
                0,
                PREPEND,
                len(tab_dict["label"]),
                Colors.YELLOW_BLACK | curses.A_BOLD,
            )

        tab_dict["tab"].refresh()


def _reposItems(stdscr, tab, selected_item, decrement=False):
    Colors = colors()
    PREPEND, REPOS_TAB_WIDTH, UPDATES_TAB_WIDTH = tabs(stdscr)

    if decrement == False:
        selected_item += 1
    else:
        selected_item -= 1

    if selected_item < 0:
        selected_item = len(_getValidRepos()) - 1
    elif selected_item > len(_getValidRepos()) - 1:
        selected_item = 0

    for i in range(len(_getValidRepos())):
        if i == selected_item:
            tab.chgat(i + 2, PREPEND, REPOS_TAB_WIDTH - 2 * PREPEND, Colors.BLACK_WHITE)
        else:
            tab.chgat(i + 2, PREPEND, REPOS_TAB_WIDTH - 2 * PREPEND, Colors.WHITE_BLACK)

    tab.refresh()

    return selected_item


def fillUpdates(tab, pad, Repo):
    Colors = colors()

    repo = Repo()
    repo.query()
    updates = repo.updates[["Packet", "Version"]].to_string()
    # updates = repo.updates.to_string()
    len_updates = len(updates.split("\n"))

    _, max_x = tab.getmaxyx()
    # tab.addstr(2, 2, f"{max_y}")
    # tab.addstr(3, 2, f"{max_x}")
    # pad = curses.newpad(max_y - 2, max_x - 2)
    # pad = curses.newpad(len(updates), max_x - 2)
    # tab.refresh()

    _, tab_x = tab.getbegyx()
    tab_max_y, tab_max_x = tab.getmaxyx()

    pad.erase()
    pad.refresh(0, 0, 1, tab_x + 3, tab_max_y - 3, tab_x + tab_max_x - 4)
    pad.resize(len_updates, max_x - 2)

    # pad.addstr(0, 0, f"{tab_y}")
    # pad.addstr(1, 0, f"{tab_x}")

    # pad.bkgd(" ", Colors.WHITE_BLUE)

    for i, update in enumerate(updates.split("\n")):
        if i == 0:
            pad.addstr(0, 0, update, curses.A_BOLD)
        else:
            pad.addstr(i, 0, update)

    pad.refresh(0, 0, 1, tab_x + 3, tab_max_y - 3, tab_x + tab_max_x - 4)


def _selectUpdate(stdscr, tab, pad, selected_update, decrement=False):
    if decrement == False:
        selected_update += 1
    else:
        selected_update -= 1

    tab_y, tab_x = tab.getbegyx()
    tab_max_y, tab_max_x = tab.getmaxyx()
    pad_max_y, pad_max_x = pad.getmaxyx()

    if selected_update < 0:
        selected_update = 0
    elif pad_max_y < tab_max_y:
        selected_update = 0
    elif selected_update > pad_max_y - (tab_max_y - 4):
        selected_update = pad_max_y - (tab_max_y - 4)

    pad.refresh(
        selected_update + 1,
        0,
        2,
        tab_x + 3,
        tab_y + tab_max_y - 3,
        tab_x + tab_max_x - 4,
    )

    return selected_update


class Keys(EnumMeta):
    LEFT = (260, 546, "h", "H")
    DOWN = (258, 526, "j", "J")
    UP = (259, 567, "k", "K")
    RIGHT = (261, 561, "l", "L")
    ENTER = ("\n", "\r\n")
    TAB = ("\t",)


def main(stdscr):
    selected_tab = -1
    selected_item = -1
    selected_update = -1

    stdscr.erase()
    curses.curs_set(0)
    if curses.has_colors:
        curses.start_color()

    repos_tab, repos_pad = reposTab(stdscr)
    updates_tab, updates_pad = updatesTab(stdscr)
    tab_dict_list = [
        {
            "label": Strings.REPOSITORIES,
            "tab": repos_tab,
            "pad": repos_pad,
        },
        {
            "label": Strings.UPDATES,
            "tab": updates_tab,
            "pad": updates_pad,
        },
    ]

    repos = _getValidRepos()

    while True:
        key = stdscr.getch()

        if chr(key).upper() == "Q":
            return

        elif key in Keys.DOWN or chr(key) in Keys.DOWN:
            if selected_tab == 0:
                tab = tab_dict_list[0]["tab"]
                selected_item = _reposItems(stdscr, tab, selected_item)
            elif selected_tab == 1:
                tab = tab_dict_list[1]["tab"]
                pad = tab_dict_list[1]["pad"]
                selected_update = _selectUpdate(stdscr, tab, pad, selected_update)

        elif key in Keys.UP or chr(key) in Keys.UP:
            if selected_tab == 0:
                tab = tab_dict_list[0]["tab"]
                selected_item = _reposItems(stdscr, tab, selected_item, True)
            elif selected_tab == 1:
                tab = tab_dict_list[1]["tab"]
                pad = tab_dict_list[1]["pad"]
                selected_update = _selectUpdate(stdscr, tab, pad, selected_update, True)

        elif key in Keys.TAB or chr(key) in Keys.TAB:
            if selected_tab < len(tab_dict_list) - 1:
                selected_tab += 1
            else:
                selected_tab = -1
            _selectTab(stdscr, tab_dict_list, selected_tab)

        elif key in Keys.ENTER or chr(key) in Keys.ENTER:
            if selected_tab == 0 and selected_item >= 0 and selected_item <= len(repos):
                fillUpdates(
                    tab_dict_list[1]["tab"],
                    tab_dict_list[1]["pad"],
                    repos[selected_item],
                )

        elif key in Keys.LEFT or chr(key) in Keys.LEFT:
            selected_tab = 0
            _selectTab(stdscr, tab_dict_list, selected_tab)

        elif key in Keys.RIGHT or chr(key) in Keys.RIGHT:
            selected_tab = 1
            _selectTab(stdscr, tab_dict_list, selected_tab)

        if False:
            if key:
                tab_dict_list[1]["tab"].addstr(10, 10, f"'{key}'")
                tab_dict_list[1]["tab"].addstr(11, 10, f"'{chr(key)}'")
                tab_dict_list[1]["tab"].refresh()

        sleep(0.025)


if __name__ == "__main__":
    curses.wrapper(main)
