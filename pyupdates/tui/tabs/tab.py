#! /usr/bin/env python3


import curses
from abc import ABC, abstractmethod
from typing import Callable, Optional, overload

from pyupdates.tui import coordinates
from pyupdates.tui.colors import getColors


class CursesString:
    def __init__(self, y: int, x: int, string: str, attribute=None) -> None:
        self.__y = y
        self.__x = x
        self.__string = string
        self.__attribute = attribute

    def __hash__(self) -> int:
        return hash((self.y, self.x, self.string))

    @property
    def y(self):
        return self.__y

    @property
    def x(self):
        return self.__x

    @property
    def string(self):
        return self.__string

    @property
    def attribute(self):
        return self.__attribute


class CursesNString(CursesString):
    def __init__(
        self, y: int, x: int, string: str, attribute=None, length=None
    ) -> None:
        self.__length = length
        super().__init__(y, x, string, attribute)

    @property
    def length(self):
        return self.__length


class Tab(ABC):
    __width_range = range(20, 51)
    __width_percent = 1 / 4
    _name: str

    def __init__(
        self,
        stdscr,
        pad,
        getOrigo: Callable,
        getCorner: Callable,
    ) -> None:
        self._strings: list[CursesString] = []
        self.__prepend = coordinates.Point(2, 3)
        # self._stdscr_corner = coordinates.Point(*stdscr.getmaxyx())
        self._pad = pad
        self.__getOrigo = getOrigo
        self.__getCorner = getCorner
        self.resize(stdscr)
        self.refresh()
        self._colors = getColors()

    def _getMainSplitX(self) -> int:
        x = int(
            min(
                max(
                    self._stdscr_corner.x * self.__width_percent,
                    min(self.__width_range),
                ),
                max(self.__width_range),
            )
        )
        return x

    def refresh(self) -> None:
        # if self._strings:
        #    for string in self._strings:
        #        self.addstr(string.y, string.x, string.string, string.attribute)

        self._pad.refresh(
            0,
            0,
            self.coordinates.origo.y,
            self.coordinates.origo.x,
            self.coordinates.corner.y,
            self.coordinates.corner.x,
        )

    def resize(self, stdscr) -> None:
        self._stdscr = stdscr
        self._stdscr_corner = coordinates.Point(*stdscr.getmaxyx())
        self._origo = self.__getOrigo()
        self._corner = self.__getCorner()

        self._pad.erase()
        self._pad.resize(self.size.y, self.size.x)
        self._pad.border()

    def hasResized(self, stdscr) -> bool:
        if self._stdscr_corner == stdscr.getmaxyx():
            return False
        else:
            return True
        # return True if self._stdscr_corner == stdscr.getmaxyx() else False

    def addstr(self, y: int, x: int, string: str, attribute: Optional[int] = None):
        curses_string = CursesString(y, x, string, attribute)

        if curses_string not in self._strings:
            self._strings.append(curses_string)

        if attribute is None:
            self._pad.addstr(curses_string.y, curses_string.x, curses_string.string)
        else:
            self._pad.addstr(
                curses_string.y, curses_string.x, curses_string.string, attribute
            )

    @abstractmethod
    def initContent(self):
        self.addstr(
            0, self.prepend.x, self._name, self._colors.YELLOW_BLACK | curses.A_BOLD
        )

    @property
    def size(self) -> coordinates.Point:
        # return self._corner - self._origo
        rectangle = coordinates.Rectangle(origo=self._origo, corner=self._corner)
        return rectangle.size

    @property
    def prepend(self):
        return self.__prepend

    @property
    def coordinates(self):
        return coordinates.Rectangle(self._origo, self._corner)
