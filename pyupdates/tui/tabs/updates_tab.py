#! /usr/bin/env python3


from pyupdates.repos.repo_enum import RepoEnum
from pyupdates.tui import coordinates
from pyupdates.tui.tabs.tab import Tab


class UpdatesTab(Tab):
    _name = "Updates"

    def __init__(self, stdscr, pad) -> None:
        super().__init__(
            stdscr,
            pad,
            self.__getOrigo,
            lambda: coordinates.Point(*self._stdscr.getmaxyx()),
        )

    def __getOrigo(self) -> coordinates.Point:
        return coordinates.Point(0, self._getMainSplitX())

    def initContent(self):
        super().initContent()
        self.refresh()
