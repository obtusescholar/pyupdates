#! /usr/bin/env python3


from pyupdates.repos.repo_enum import RepoEnum
from pyupdates.tui import coordinates
from pyupdates.tui.tabs.tab import Tab


class ReposTab(Tab):
    _name = "Repositories"

    def __init__(self, stdscr, pad) -> None:
        self.__repos = RepoEnum.getValid()
        self.__width_range = range(20, 51)
        super().__init__(stdscr, pad, lambda: coordinates.Point(0, 0), self.__getCorner)

    def __getCorner(self) -> coordinates.Point:
        return coordinates.Point(self._stdscr_corner.y, self._getMainSplitX())

    def initContent(self):
        super().initContent()
        for i, repo in enumerate(self.__repos):
            self.addstr(self.prepend.y + i, self.prepend.x, repo.name.capitalize())
        self.refresh()
