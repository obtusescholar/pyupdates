#! /usr/bin/env python3


from enum import EnumMeta

from pyupdates.repos.repo_enum import RepoEnum


class Strings(EnumMeta):
    TITLE_ALL = "All"
    TITLE_REPOS = "Repositories"
    TITLE_UPDATES = "Updates"
    REPO_NAMES = [repo_enum.value.name for repo_enum in RepoEnum if repo_enum.isValid()]
