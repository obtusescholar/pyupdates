#! /usr/bin/env python3


import curses
from enum import EnumMeta


def getColors():
    curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_YELLOW)
    curses.init_pair(5, curses.COLOR_BLACK, curses.COLOR_WHITE)

    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLACK)

    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_WHITE, curses.COLOR_YELLOW)
    curses.init_pair(7, curses.COLOR_WHITE, curses.COLOR_BLUE)

    class CursesColors(EnumMeta):
        WHITE_BLACK = curses.color_pair(1)
        WHITE_YELLOW = curses.color_pair(4)
        WHITE_BLUE = curses.color_pair(5)

        BLACK_WHITE = curses.color_pair(7)
        BLACK_YELLOW = curses.color_pair(6)

        YELLOW_BLACK = curses.color_pair(2)
        BLUE_BLACK = curses.color_pair(3)

    return CursesColors
