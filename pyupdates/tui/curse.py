#! /usr/bin/env python3


import curses


class Stdscr:
    def __init__(self) -> None:
        self.stdscr = None

    def __enter__(self):
        try:
            self.open()
            return self.stdscr
        except:
            self.close()
            raise

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close()

    def open(self):
        self.stdscr = curses.initscr()

        self.stdscr.nodelay(True)

        curses.curs_set(0)

        curses.noecho()
        curses.cbreak()

        self.stdscr.keypad(True)

        try:
            curses.start_color()
        except:
            pass

    def close(self):
        if self.stdscr is not None:
            curses.curs_set(1)
            self.stdscr.keypad(False)
            curses.echo()
            curses.nocbreak()
            curses.endwin()
