#! /usr/bin/env python3


from typing import Iterator


class Point:
    def __init__(self, y: int, x: int) -> None:
        self.__y = y
        self.__x = x

    @property
    def y(self) -> int:
        return self.__y

    @property
    def x(self) -> int:
        return self.__x

    def __sub__(self, other):
        if isinstance(other, Point):
            self.__y -= other.y
            self.__x -= other.x
            return self
        else:
            raise NotImplementedError(
                f"__sub__ not implemented for type:'{type(other)}'."
            )

    def __add__(self, other):
        if isinstance(other, Point):
            self.__y += other.y
            self.__x += other.x
            return self
        else:
            raise NotImplementedError(
                f"__add__ not implemented for type:'{type(other)}'."
            )

    def __eq__(self, other) -> bool:
        if isinstance(other, Point):
            return self.y == other.y and self.x == other.x
        elif isinstance(other, tuple):
            return self.y == other[0] and self.x == other[1]
        else:
            raise NotImplementedError(
                f"Point __eq__ not implemented for '{type(other)}'."
            )

    def __repr__(self) -> str:
        return f"Point(y={self.y}, x={self.x})"

    def __iter__(self) -> Iterator:
        return iter((self.y, self.x))
