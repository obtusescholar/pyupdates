#! /usr/bin/env python3


from pyupdates.tui.coordinates.point import Point


class Rectangle:
    def __init__(self, origo: Point, corner: Point) -> None:
        self.__origo = origo
        self.__corner = corner

    @property
    def origo(self) -> Point:
        return self.__origo

    @property
    def corner(self) -> Point:
        return self.__corner

    @property
    def size(self) -> Point:
        y = self.corner.y - self.origo.y
        x = self.corner.x - self.origo.x
        return Point(y=y, x=x)

    def __repr__(self):
        return f"Rectangle(origo={self.origo}, corner={self.corner})"
