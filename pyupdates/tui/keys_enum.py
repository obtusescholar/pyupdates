#! /usr/bin/env python3


from enum import EnumMeta


class Keys(EnumMeta):
    LEFT = (260, 546, "h", "H")
    DOWN = (258, 526, "j", "J")
    UP = (259, 567, "k", "K")
    RIGHT = (261, 561, "l", "L")
    ENTER = ("\n", "\r\n")
    TAB = ("\t",)
    QUIT = (113, 813, "q", "Q")
