#! /usr/bin/env python3


import logging
from enum import Enum, EnumMeta
from os import path
from pathlib import Path

from pytools import logtools
from pytools.string_format import StringFormat

__version__ = "0.1.0"
__url__ = ""
__author__ = "obtusescholar"
__email__ = ""


class Dirs(EnumMeta):
    PYTHON_ROOT = Path(path.dirname(path.dirname(path.abspath(__file__))))
    PROJECT_ROOT = PYTHON_ROOT


class Log(EnumMeta):
    LOG_FOLDER = Dirs.PROJECT_ROOT / "logs"
    FILE_LOGLEVEL = logging.NOTSET
    FILE_LOGFORMAT = logging.Formatter(
        fmt="%(asctime)s - %(name)s - [%(levelname)-8s] - %(message)s"
    )
    CONSOLE_LOGLEVEL = logging.WARNING
    CONSOLE_LOGFORMAT = logging.Formatter(
        fmt="%(asctime)s - %(name)s - [%(levelname)-8s] - %(message)s"
    )


class ColorRepoEnum(Enum):
    DNF = StringFormat.PURPLE
    FLATPAK = StringFormat.CYAN
    SNAP = StringFormat.RED
    APT = StringFormat.YELLOW


def logtoolsSettings() -> None:
    # folder
    logtools.DefaultSettings.logs_path = Log.LOG_FOLDER

    # log level
    logtools.DefaultSettings.filehandler_level = Log.FILE_LOGLEVEL
    logtools.DefaultSettings.streamhandler_level = Log.CONSOLE_LOGLEVEL

    # log format
    logtools.DefaultSettings.filehandler_format = Log.FILE_LOGFORMAT
    logtools.DefaultSettings.streamhandler_format = Log.CONSOLE_LOGFORMAT


logtoolsSettings()
