#! /usr/bin/env python3


from setuptools import setup

from pyupdates import definitions

setup(
    name="pyupdates",
    version=definitions.__version__,
    description="Query updates from Linux repositories.",
    url="",
    author=definitions.__author__,
    author_email=definitions.__email__,
    license="GPLv3",
    packages=["pyupdates"],
    zip_safe=False,
    entry_points={"console_scripts": {"pyupdates = pyupdates.cli:main"}},
    install_requires=["pandas"],
    extras_require={"dev": {"mypy", "black"}},
)
